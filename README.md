# Project to test a K3D cluster with Gitlab CI

## 1) check AWS identity

`$ aws sts get-caller-identity`

If theres no Docker installed :

`$ PS C:\windows\system32> wsl -l -v`
if the output shows:

```yml
* rancher-desktop         Running         2
  rancher-desktop-data    Stopped         2
  Ubuntu-22.04            Running         1
```
run the following command:

`$ wsl --set-version Ubuntu-22.04 2`

`$ sudo apt-get update`

Install using the repository:

Set up the repository:
Update the apt package index and install packages to allow apt to use a repository over HTTPS:

```yml
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```
Add Docker’s official GPG key:

`$ sudo mkdir -p /etc/apt/keyrings`

`$  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg`

Use the following command to set up the repository:

```yml
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
Update the apt package index, and install the latest version of Docker Engine, containerd, and Docker Compose

`$ sudo apt-get update`

`$ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin`

Verify that Docker Engine is installed correctly by running the hello-world image.

`$ sudo docker run hello-world`
```yml
If you are using Ubuntu inside Windows using WSL, you will have SysV instead of systemd and your system will complain when you run the systemctl command (intended for Linux systems with systemd init system).

How to know which init system you are using? You may use this command to know the process name associated with PID 1 (the first process that runs on your system):

ps -p 1 -o comm=
It should show init or sysv (or something like that) in the output. If you see init, your system is not using systemd and you should use the init commands :

Systemd command	Sysvinit command
systemctl start service_name	service service_name start
systemctl stop service_name	    service service_name stop
systemctl restart service_name	service service_name restart
systemctl status service_name	service service_name status
systemctl enable service_name	chkconfig service_name on
systemctl disable service_name	chkconfig service_name off
```

### SOLUTION
`$ sudo update-alternatives --set iptables /usr/sbin/iptables-legacy`

`$ docker ps`


## 2) create k3d cluster


`$ k3d cluster create wslmegamind -p "80:80@loadbalancer"`


## Create Gitlab Agent

```yml
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install testagent gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.4.0 \
    --set config.token=pUJFT17waDbAzU-Z-sQeCmaxonZzxA4mEwtx5xCpjx-2FFt5sQ \
    --set config.kasAddress=wss://kas.gitlab.com
```
## Install a Runner

```yml
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
Command to register runner
sudo gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941cWfUN2v7Yy1QxY-h4QPq
```

## References
- https://docs.docker.com/engine/install/ubuntu/

commit 3